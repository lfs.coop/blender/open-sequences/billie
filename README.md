# Billie

Billie is a 2D animated sequence made with Blender Grease Pencil. It's made to be a demo scene for research around 2D animation. Anyone can use it to learn or test new tools on it, and share demos.

![UI](docs/billie_preview.jpg "Billie preview image")


## Content

The animation is a cycle, containing 3 sub-loops so you can focus your demo on a specific part if needed. Check the marks in the timeline within the Blender file. It was made for Blender 3.4 and up.

The Blender scene comes with all the steps in it:
- **ROUGH**: first rough steps of animation, including notes and doodles
- **TIE-DOWN**: a clean tie-down of the animation
- **COLOR-REF**: just one clean keyframe used to set the materials needed to Ink and Paint
- **INK-PAINT**: the final drawing (the one displayed by default)
- **LONGBOARD**: the only 3D prop. The Ink-Paint model is parented to it, but you can mute its animation if needed.
- **SETS/SKY**: the set, with an array modifier 

## Credits

Les Fées Spéciales

- Design: Léa Cluzel
- Animation: Camille Guillot
- Ink&Paint: Coline Fournier
- Technical Support: Damien Picard
- Production Manager: Natalene Darfeuille
- Administration: Gabriela de Carvalho
- Supervision: Amélie Fondevilla, Flavio Perez & Eric Serre

This scene is part of the our RnD program "Grease Pencil Powertools", made possible **with the support of the CNC**.

## License

Billie © 2023 by Les Fées Spéciales is licensed under CC BY 4.0. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/

This license enables reusers to distribute, remix, adapt, and build upon the material in any medium or format, so long as attribution is given to the creator. The license allows for commercial use. CC BY includes the following elements:

CC BY: Les Fées Spéciales.