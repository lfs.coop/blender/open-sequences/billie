import bpy

''' 
    Example template file to manipulate grease pencil animation data from Blender.
    See the API documentation for more complex treatment : https://docs.blender.org/api/current/.
'''

ob_name = "Billie_ink_paint" # Name of the object to export from
active_layer_only = True
current_frame_only = True

ob = bpy.data.objects[ob_name]

''' Transformation matrix for the object ''' 
mat = ob.matrix_world

gp = ob.data
layers = [gp.layers.active] if active_layer_only else gp.layers

''' Loop through the layers of the object. '''
for layer in layers:
    if layer is None:
        continue
    
    frames = [layer.active_frame] if current_frame_only else layer.frames
    
    ''' Loop through the frames of the layer. '''
    for frame in frames:
        if frame is None:
            continue
        
        ''' Loop through the strokes of the frame. '''
        for stroke in frames.strokes:
            
            ''' Here is the list of 3D coordinates in local (object) space. '''
            points_coordinates = [p.co for p in stroke.points]

            
            ''' Here is the list of 3D coordinates in world space. '''
            points_coordinates = [mat @ p.co for p in stroke.points]